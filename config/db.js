const config = require('../knexfile.js')
const knex = require('knex')(config)

// executa as migrations todas as vezes que o sistema carrega
knex.migrate.latest([config])

module.exports = knex