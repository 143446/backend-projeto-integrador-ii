module.exports = app => {
  const {existsOrError, notExistsOrError} = app.api.validation

  const save = (req, res) => {
    const category = { ...req.body }
    if(req.params.id) category.id = req.params.id

    try {
      existsOrError(category.name, 'Nome não informado')
    } catch(msg) {
      return res.status(400).send(msg)
    }

    if(category.id) {
      app.db('categories')
        .update(category)
        .where({ id: category.id })
        .then(_ => res.status(204).send())
        .catch(err => res.status(500).send(err))
    } else {
      app.db('categories')
        .insert(category)
        .then(_ => res.status(204).send())
        .catch(err => res.status(500).send(err))
    }
  }

  // se categorias tem subcategorias ou está associada a artigos, não pode ser removida
  const remove = async (req, res) => {
    try {
      existsOrError(req.params.id, 'Código da Categoria não foi informado.')

      const subcategory = await app.db('categories')
        .where({ parentId: req.params.id })

      notExistsOrError(subcategory, 'Categoria possui subcategorias.')

      const articles = await app.db('articles')
        .where({ categoryId: req.params.id })
      notExistsOrError(articles, 'Categoria possui artigos.')

      const rowsDeleted = await app.db('categories')
        .where({ id: req.params.id }).del()
      existsOrError(rowsDeleted, 'Categoria não foi encontrada.')

      res.status(204).send()
    } catch(msg) {
      res.status(400).send(msg)
    }
  }

  // Organização dos caminhos das categorias e subcategorias que serão listados
  // Lista de categorias mais atributo path
  // Primeira função: filtra o parent que está procurando da categoria pai e verifica se o id é exatamente igual
  // Segunda função: faz um map e transforma um array de categorias em outro array de categorias acrescentando um atributo a mais chamado path

  const withPath = categories => {
    const getParent = (categories, parentId) => {
      const parent = categories.filter(parent => parent.id === parentId)
      return parent.length ? parent[0] : null
    }

    const categoriesWithPath = categories.map(category => {
      // se existir perent
      let path = category.name
      let parent = getParent(categories, category.parentId)
      
      // enquanro existir continua procurando parent, até o ultimo nó que não tem mais pai e assim vai montando o caminho completo
      while(parent) {
        path = `${parent.name} > ${path}`
        parent= getParent(categories, parent.parentId)
      }

      return { ...category, path }
    })

    // Ordenar a listagem em ordem alfabética pelo caminho

    categoriesWithPath.sort((a, b) => {
      if(a.path < b.path) return -1
      if(a.path > b.path) return 1
      return 0
    })

    return categoriesWithPath
  }

  const get = (req, res) => {
    app.db('categories')
      .then(categories => res.json(withPath(categories)))
      .catch(err => res.status(500).send(err))
  }

  const getById = (req, res) => {
    app.db('categories')
      .where({ id: req.params.id })
      .first()
      .then(category => res.json(category))
      .catch(err => res.status(500).send(err))
  }

  // Montando MENU na estrutura de árvore
  const toTree = (categories, tree) => {
    // gera a partir das categorias que não tem pai(parentId não setado)
    if(!tree) tree = categories.filter(c => !c.parentId)
    // fazer uma transformação na árvore encontrando os filhos de parentNode(pai), se o id for igual o nó é filho 
    tree = tree.map(parentNode => {
      const isChild = node => node.parentId == parentNode.id
      parentNode.children = toTree(categories, categories.filter(isChild))
      return parentNode
    })
    return tree
  }

  const getTree = (req, res) => {
    app.db('categories')
      .then(categories => res.json(toTree(withPath(categories))))
      .catch(err => res.status(500).send(err))
  }
  return { save, remove, get, getById, getTree }
}