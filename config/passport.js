const { authSecret } = require('../.env')
const passport = require('passport') // framework para validar token
const passportJwt = require('passport-jwt')
const { Strategy, ExtractJwt } = passportJwt // extractJwt pega o cabeçalho da requisição

module.exports = app => {
    const params = {
        secretOrKey: authSecret, // segredo para decodificar token
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken() // procura no cabeçalho da requisição e extrai token
    }

    const strategy = new Strategy(params, (payload, done) => {
        app.db('users')
            .where({ id: payload.id })
            .first()
            .then(user => done(null, user ? { ...payload } : false)) 
            .catch(err => done(err, false))
    })

    passport.use(strategy)

    return {
        authenticate: () => passport.authenticate('jwt', { session: false }) // usar nas routes
    }
}