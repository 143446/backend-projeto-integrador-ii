const queries = require('./queries')

module.exports = app => {
  const {existsOrError} = app.api.validation

  const save = (req, res) => {
    const article = { ...req.body }
    if(req.params.id) article.id = req.params.id

    try {
      existsOrError(article.name, 'Nome não informado')
      existsOrError(article.description, 'Descrição não informada')
      existsOrError(article.categoryId, 'Categoria não informada')
      existsOrError(article.userId, 'Autor não informado')
      existsOrError(article.content, 'Conteúdo não informado')
    } catch(msg) {
      res.status(400).send(msg)
    }

    if(article.id) {
      app.db('articles')
      .update(article)
        .where({ id: article.id })
        .then(_ => res.status(204).send())
        .catch(err => res.status(500).send(err))
    } else {
      app.db('articles')
        .insert(article)
        .then(_ => res.status(204).send())
        .catch(err => res.status(500).send(err))
    }
  }

  const remove = async (req, res) => {
    try {
      const rowsDeleted = await app.db('articles')
        .where({ id: req.params.id }).del()
      try {
        existsOrError(rowsDeleted, 'Artigo não foi encontrado.')
      } catch(msg) {
        return res.status(400).send(msg)
      } 

      res.status(204).send()
  } catch(msg) {
      res.status(500).send(msg)
    }
  }

  const limit = 10 // usado para paginação
  const get = async (req, res) => {
    const page = req.query.page || 1 // espera a página que foi setada, ou então pega a 1 por padrão

    const result = await app.db('articles').count('id').first()
    const count = parseInt(result.count)

    app.db('articles')
      .select('id', 'name', 'description')
      .limit(limit).offset(page * limit - limit) // a partir dá onde faz a consulta, deslocamento para retornar os dados e mostrar na tela
      .then(articles => res.json({ data: articles, count, limit }))  //em uma unica consulta tem todas as informações pra renderizar o conteudo e o paginador
      .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
      app.db('articles')
        .where({ id: req.params.id })
        .first()
        .then(article => {
          article.content = article.content.toString() // converte de binario para string
          return res.json(article)
        })
        .catch(err => res.status(500).send(err))
    }

    // consulta recursiva com with, pegando todos os artigos com os ids que pertencem as categorias filhas
    const getByCategory = async (req, res) => {
      const categoryId = req.params.id // id da categoria
      const page = req.query.page || 1
      const categories = await app.db.raw(queries.categoryWithChildren, categoryId)
      const ids = categories.rows.map(c => c.id) // id das categorias filhas

      app.db({a: 'articles', u: 'users'}) // consulta de duas tabelas difetentes, artigos e usuários
          .select('a.id', 'a.name', 'a.description', 'a.imageUrl', { author: 'u.name' })
          .limit(limit).offset(page * limit - limit)
          .whereRaw('?? = ??', ['u.id', 'a.userId']) // igualar as duas tabelas
          .whereIn('categoryId', ids) // todos id que pertencem a consulta
          .orderBy('a.id', 'desc')
          .then(articles => res.json(articles))
          .catch(err => res.status(500).send(err))
  }
    return { save, remove, get, getById, getByCategory }
}