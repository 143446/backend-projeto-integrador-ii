const { authSecret } = require('../.env')
const jwt = require('jwt-simple')
const bcrypt = require('bcrypt-nodejs') // comparar senhas do banco com a que recebi pra fazer login

module.exports = app => {
    const signin = async (req, res) => {
        if (!req.body.email || !req.body.password) { // valida email e senha
            return res.status(400).send('Informe usuário e senha!') // se um dos dois não for informado
        }

        const user = await app.db('users') // pega email do banco
            .where({ email: req.body.email })
            .first()

        if (!user) return res.status(400).send('Usuário não encontrado!') // se não existir

        const isMatch = bcrypt.compareSync(req.body.password, user.password) //compara senhas
        if (!isMatch) return res.status(401).send('Email/Senha inválidos!')

        const now = Math.floor(Date.now() / 1000) // pega data atual em segundos para saber em que momento foi criado o token

        const payload = {
            id: user.id,
            name: user.name,
            email: user.email,
            admin: user.admin,
            iat: now, // data de geração do token
            exp: now + (60 * 60 * 24 * 3) // validade do token - min * seg * dia(em horas) * quant de dias = vale 3 dias
        }

        res.json({ // resposta
            ...payload,
            token: jwt.encode(payload, authSecret)
        })
    }

    const validateToken = async (req, res) => {
        const userData = req.body || null
        try {
            if(userData) {
                const token = jwt.decode(userData.token, authSecret)
                if(new Date(token.exp * 1000) > new Date()) { // se a data de geração for maior que a data atual é válido
                    return res.send(true)
                }
            }
        } catch(e) {
            // problema com o token
        }

        res.send(false)
    }

    return { signin, validateToken }
}