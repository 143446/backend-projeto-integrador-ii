const bodyParser = require('body-parser') // interpreta o body
const cors = require('cors')

module.exports = app => {
    app.use(bodyParser.json())
    app.use(cors())
}